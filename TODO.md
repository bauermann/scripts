# TODO

Pull requests welcome! :-)

## dial

- [ ] Use environment variable to find DTMF files instead of assuming $PWD.

## get-video

- [ ] Convert to Python 3.
- [ ] Make video title an optional argument.

## notify-machine-online

- [ ] Add command line option to select between IPv4 and IPv6.
  - [ ] Better yet, auto-detect IPv4 or IPv6.
