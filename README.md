# Scripts

These are random scripts that I use frequently and could be potentially useful
to others as well.

## dial

Plays the DTMF tones for the phone number given as an argument.
Useful when your phone's keypad is defective. :-)

Uses the DTMF tones found in [Wikipedia](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling#Keypad).

## firefox-activity

Opens Firefox with the right profile for the current KDE activity.

To use it, change the Firefox launcher to point to this script.
By default, it will use a profile with the same name as the KDE activity.
You can change the activity → profile mapping or the Firefox binary
by creating a configuration file like the following one:

```
$ cat ~/.config/firefox-activity.conf
[config]
firefox_binary = /path/to/firefox

[profiles]
activity 1 = default
activity 2 = alternate firefox profile
```

If ```XDG_CONFIG_HOME``` is set, it will be used instead of ~/.config.
All sections and configuration parameters are optional.

This script depends on the PyXDG library. On Debian and Ubuntu, you can
get it with:

```
$ sudo apt-get install python3-xdg
```

## get-video

Given URL of the video and its title, add it to the git annex repository
in the current directory.

## notify-machine-online

Pings the given machine and shows a desktop notification if it's online.

Suggested usage:

```
$ crontab -l
*/15 * * * *    /home/user/scripts.git/notify-machine-online foo.example.net
```
